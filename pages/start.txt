# About
This wiki is supposed to document my knowledge and setup so I don't have to look things up twice. Since you stumbled upon it, I hope it helps you! 

Keep in mind that the wiki is permanently under construction. Information might be wrong, incomplete, ... I'm not exactly getting paid to keep it up to date.

Backup of the wiki's contents [is here](https://gitlab.com/C0rn3j/wiki).

# List of all pages

* My:
  * [[CV]]
  * [[Hardware]]
  * [[Software]]
  * [[Services]]
    * [[Minecraft]]
  * [[Recipes]]
  * [[RSS/Atom feeds]]
  * [[Wishlist]]
* [[Interesting links]]
* Operating systems:
  * [[Arch Linux]]
  * [[Windows]]
* [[webOS]] - LG TV OS
* How to's:
  * [[Converting Wii's Bluetooth module to a USB dongle]] - for perfect Wii motes connection to the Dolphin emulator
  * [[Creating a bootable flashdrive]]
  * [[Electronics at component level]]
  * [[Geopricing]]
  * [[Offensive]] ​- Attacking Wi-Fi, Rubber ducky, ...
  * [[Raspberry Pi as fake mass storage]]
  * [[System Administration]]
  * [[System Basics]]
  * [[Video Encoding]]




# Contact me

If you wish to contact me, you can do so at following places:

* Email (and Hangouts) - spleefer90@gmail.com
* Matrix([Riot](https://riot.im/app)) - @C0rn3j:matrix.org - Am rarely active there
* Steam - [C0rn3j](https://steamcommunity.com/id/c0rn3j/)
* Telegram - [@C0rn3j](https://t.me/C0rn3j)

You will get the fastest response via Telegram and email.

SSH pubkey:
  ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIN0UrYQJE+udiy4LldhUIzfuaKM6F3wBUV/CjQwMaksF c0rn3j@c0rn3jDesktop

# Donate


If I saved you some time, you can send a pizza my way by [PayPal](https://www.paypal.me/MartinRys/5usd) or pledge on the ghost town that is my [Patreon](https://www.patreon.com/C0rn3j) account.

---

# Trash

* [[Cryptocurrencies]]
* [[Matrix/Riot]]
* [[Kindle FW]] - Hush little baby
* [[Test page]], [[Test page 2]]
* [[IntwTask]]
* Redirects: [[Operating systems]], [[Software and services I use]]